set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'rust-lang/rust.vim'
Plugin 'vhdirk/vim-cmake'
Bundle 'Valloric/YouCompleteMe'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'dracula/vim'
Plugin 'mphe/grayout.vim'
call vundle#end()
filetype plugin indent on

" Set racer cmd
let g:racer_cmd = "~/.cargo/bin/racer"
let g:racer_experimental_completer = 1

set tabstop=4
set shiftwidth=4
set expandtab
set showmatch
set autoindent
syntax enable
syntax on
set encoding=utf-8
set ruler
set background=dark

" Set vertical guide at 100
set textwidth=100
set formatoptions-=t
" Colour position 81
set colorcolumn=+1

" Fix weir term input with autoclose plugin
let g:AutoClosePreserveDotReg = 0

"Set theme
let g:gruvbox_contrast_dark="hard"
colorscheme dracula

" Set Gui options
if has("gui_running")
    " GUI only options
    set antialias
    set guioptions=agi
else
    " Terminal only options
endif

" Assume all formats are decimal
set nrformats=

" Enable suggestion navigation in command mode
set wildmode=longest,list

" Increse vim's history limit
set history=200

" Remap keys for fast buffer nav
nnoremap <silent> [b :bprevious<CR>
nnoremap <silent> ]b :bnext<CR>
nnoremap <silent> [B :bfirst<CR>
nnoremap <silent> ]B :blast<CR>

" Enable spell checking
set spell spelllang=en_us

" YcmComplete key
nnoremap <leader>gt :YcmCompleter GoTo<CR>

" Highlight search results
set incsearch

" Allow to switch buffers without saving
set hidden

" Show commands as they are being typed
set showcmd

" Use relative numbering
set relativenumber

" Load ctrlp.vim
set runtimepath^=~/.vim/bundle/ctrlp.vim
" Ignore files and directories
let g:ctrlp_custom_ignore= {
    \'dir': '\v[\/]\.(git|hg|svn)$',
    \'file': '\v\.(exe|so|dll|lib|a)$',
    \}
" Set search dir to current dir
let g:ctrlp_working_path_mode= 'ra'

" remove trailing whitespaces
autocmd BufWritePre * :%s/\s\+$//e

" Status Bar
function! GitBranch()
    return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatusLineGit()
    let l:branchname = GitBranch()
    return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

hi User1 ctermbg=233 ctermfg=white gui=bold
hi User2 ctermbg=234 ctermfg=white gui=bold
hi User3 ctermbg=236 ctermfg=white gui=bold

set laststatus=2
set statusline=
set statusline+=\ %03l
set statusline+=\ %*
"set statusline+=%#PmenuSel#
"set statusline+=%{StatusLineGit()}
"set statusline+=%#LineNr#
set statusline+=%1*
set statusline+=\ %m
set statusline+=\ %f
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=%2*
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\ [%{&fileformat}\]
set statusline+=\
set statusline+=%3*
set statusline+=\ %03p%%
set statusline+=\ %03l:%02c
set statusline+=\

" Set defaul args for YCMCompleter when nothing else is present
" let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"

" Close scratch buffer automatically
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


" Tmux fixes:
"if &term =~ '^screen' && exists('$TMUX')
"   set mouse+=a
"   " tmux knows the extended mouse mode
"    set ttymouse=xterm2
"    " tmux will send xterm-style keys when xterm-keys is on
"    execute "set <xUp>=\e[1;*A"
"    execute "set <xDown>=\e[1;*B"
"    execute "set <xRight>=\e[1;*C"
"    execute "set <xLeft>=\e[1;*D"
"    execute "set <xHome>=\e[1;*H"
"    execute "set <xEnd>=\e[1;*F"
"    execute "set <Insert>=\e[2;*~"
"    execute "set <Delete>=\e[3;*~"
"    execute "set <PageUp>=\e[5;*~"
"    execute "set <PageDown>=\e[6;*~"
"    execute "set <xF1>=\e[1;*P"
"    execute "set <xF2>=\e[1;*Q"
"    execute "set <xF3>=\e[1;*R"
"    execute "set <xF4>=\e[1;*S"
"    execute "set <F5>=\e[15;*~"
"    execute "set <F6>=\e[17;*~"
"    execute "set <F7>=\e[18;*~"
"    execute "set <F8>=\e[19;*~"
"    execute "set <F9>=\e[20;*~"
"    execute "set <F10>=\e[21;*~"
"    execute "set <F11>=\e[23;*~"
"    execute "set <F12>=\e[24;*~"
"endif

" Needed for tmux and vim to play nice
" Needed for tmux and vim to play nice
"map <Esc>[A <Up>
"map <Esc>[B <Down>
"map <Esc>[C <Right>
"map <Esc>[D <Left>
"
"" Console movement
"cmap <Esc>[A <Up>
"cmap <Esc>[B <Down>
"cmap <Esc>[C <Right>
"cmap <Esc>[D <Left>

" Grayout plugin settings
let g:grayout_libclang_path= '/usr/lib/'
" Enable debug log
let g:grayout_debug = 0
" Run on \\gr
nnoremap <leader>gr :GrayoutUpdate<CR>
